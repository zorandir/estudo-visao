package br.com.zorandir.visao.utils;

public class VisaoUtils {

	public static int[][] copyMatriz(int[][] origem) {
		int[][] matriz;
		int largura = origem.length;
		int altura = origem[0].length;
		matriz = new int[largura][altura];

		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha] = origem[coluna][linha];
			}
		}

		return matriz;
	}

	public static int[][][] copyMatriz(int[][][] origem) {
		int[][][] matriz;
		int largura = origem.length;
		int altura = origem[0].length;
		matriz = new int[largura][altura][3];

		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha][0] = origem[coluna][linha][0];
				matriz[coluna][linha][1] = origem[coluna][linha][1];
				matriz[coluna][linha][2] = origem[coluna][linha][2];
			}
		}

		return matriz;
	}

	public static void printHistograma(double[] histograma) {

		for (int i = 0; i < histograma.length; i++){
			System.out.println(histograma[i]);
		}

	}

	
	public static int limitRGB(int pixel) {

		if (pixel > 255) {
			return 255;
		} else if (pixel < 0) {
			return 0;
		} else {
			return pixel;
		}
	}

}
