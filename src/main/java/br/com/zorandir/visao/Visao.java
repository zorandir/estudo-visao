package br.com.zorandir.visao;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import br.com.zorandir.visao.utils.VisaoUtils;

public class Visao {

	// Matrizes Coloridas
	public int[][][] getMatrizRgb(String arquivo) {
		int[][][] matriz;
		try {
			BufferedImage imagem = ImageIO.read(new File(arquivo));
			matriz = getMatrizRgb(imagem);
		} catch (IOException ex) {
			matriz = new int[0][0][0];
			Logger.getLogger(Visao.class.getName()).log(Level.SEVERE, null, ex);
		}
		return matriz;
	}

	public int[][][] getMatrizRgb(BufferedImage imagem) {
		int[][][] matriz;
		int largura = imagem.getWidth();
		int altura = imagem.getHeight();
		matriz = new int[largura][altura][3];
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				Color pixel = new Color(imagem.getRGB(coluna, linha));
				matriz[coluna][linha][0] = pixel.getRed();
				matriz[coluna][linha][1] = pixel.getGreen();
				matriz[coluna][linha][2] = pixel.getBlue();
			}
		}
		return matriz;
	}

	public int[][][] getMatrizNegativa(int[][][] origem) {
		int[][][] matriz = VisaoUtils.copyMatriz(origem);
		int largura = matriz.length;
		int altura = matriz[0].length;
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha][0] = 255 - matriz[coluna][linha][0];
				matriz[coluna][linha][1] = 255 - matriz[coluna][linha][1];
				matriz[coluna][linha][2] = 255 - matriz[coluna][linha][2];
			}
		}
		return matriz;
	}

	public int[][][] getLayerVermelho(int[][][] origem) {
		int[][][] matriz = VisaoUtils.copyMatriz(origem);
		int largura = matriz.length;
		int altura = matriz[0].length;
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha][1] = 0;
				matriz[coluna][linha][2] = 0;
			}
		}
		return matriz;
	}

	public int[][][] getLayerVerde(int[][][] origem) {
		int[][][] matriz = VisaoUtils.copyMatriz(origem);
		int largura = matriz.length;
		int altura = matriz[0].length;
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha][0] = 0;
				matriz[coluna][linha][2] = 0;
			}
		}
		return matriz;
	}

	public int[][][] getLayerAzul(int[][][] origem) {
		int[][][] matriz = VisaoUtils.copyMatriz(origem);
		int largura = matriz.length;
		int altura = matriz[0].length;
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha][0] = 0;
				matriz[coluna][linha][1] = 0;
			}
		}
		return matriz;
	}

	// Matrizes em tons de cinza ou preto e branco
	public int[][] getMatrizCinza(int[][][] origem) {
		int[][] matriz;
		int largura = origem.length;
		int altura = origem[0].length;
		matriz = new int[largura][altura];
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				int cinza = (origem[coluna][linha][0] + origem[coluna][linha][1] + origem[coluna][linha][2]) / 3;
				matriz[coluna][linha] = cinza;
			}
		}
		return matriz;
	}

	public int[][] getMatrizBinariza(int[][] origem, int limiar) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);
		int largura = matriz.length;
		int altura = matriz[0].length;

		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha] = (matriz[coluna][linha] > limiar) ? 255 : 0;
			}
		}

		return matriz;
	}

	// Filtros
	public int[][] getMatrizBrilho(int[][] origem, int brilho) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);
		int largura = matriz.length;
		int altura = matriz[0].length;

		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha] += brilho;
				matriz[coluna][linha] = VisaoUtils.limitRGB(matriz[coluna][linha]);
			}
		}

		return matriz;
	}

	public int[][] getMatrizContraste(int[][] origem, double contraste) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);
		int largura = matriz.length;
		int altura = matriz[0].length;

		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				matriz[coluna][linha] *= (int) contraste;
				matriz[coluna][linha] = VisaoUtils.limitRGB(matriz[coluna][linha]);
			}
		}

		return matriz;
	}

	//Normal
	public double[] getHistograma(int[][] origem) {
		double[] histograma = new double[256];
		int largura = origem.length;
		int altura = origem[0].length;

		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				int tomDeCinza = origem[coluna][linha];
				histograma[tomDeCinza] += 1;
			}
		}

		return histograma;
	}

	//Porcentagem
	public double[] getHistogramaNormalizado(int[][] origem) {
		double[] histograma = getHistograma(origem);
		
		int largura = origem.length;
		int altura = origem[0].length;
		int total = largura * altura;
		
		for (int i = 0; i < histograma.length; i++){
			histograma[i] /= total; 
		}

		return histograma;
	}

	//Porcentagem Acumulada
	public double[] getHistogramaAcumulado(int[][] origem) {
		double[] histograma = getHistogramaNormalizado(origem);

		for (int i = 1; i < histograma.length; i++){
			histograma[i] += histograma[i-1]; 
		}

		return histograma;
	}

	public double[] getHistogramaEqualizado(int[][] origem) {
		double[] histograma = getHistogramaAcumulado(origem);

		for (int i = 1; i < histograma.length; i++){
			histograma[i] *= histograma.length;
			histograma[i] = new Double(histograma[i]).intValue(); 
		}

		return histograma;
	}

	public int[][] getMatrizFiltroPassaBaixaMedia(int[][] origem) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);

		int[][] mascara = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
		int largura = matriz.length;
		int altura = matriz[0].length;
		
		for (int linha = 1; linha < altura-1; linha++) {
			for (int coluna = 1; coluna < largura-1; coluna++) {
				matriz[coluna][linha] = (
						origem[coluna-1][linha-1]*mascara[0][0] + origem[coluna][linha-1]*mascara[0][1] + origem[coluna+1][linha-1]*mascara[0][2] + 
						origem[coluna-1][linha+0]*mascara[1][0] + origem[coluna][linha+0]*mascara[1][1] + origem[coluna+1][linha+0]*mascara[1][2] + 
						origem[coluna-1][linha+1]*mascara[2][0] + origem[coluna][linha+1]*mascara[2][1] + origem[coluna+1][linha+1]*mascara[2][2]) / 9;
			}
		}

		return matriz;
	}

	public int[][] getMatrizFiltroEqualizador(int[][] origem) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);

		double[] histogramaEqualizado = getHistogramaEqualizado(matriz);

		int largura = matriz.length;
		int altura = matriz[0].length;
		
		for (int linha = 1; linha < altura-1; linha++) {
			for (int coluna = 1; coluna < largura-1; coluna++) {
				int i = matriz[coluna][linha];
				Double double1 = new Double(histogramaEqualizado[i]);
				matriz[coluna][linha] = VisaoUtils.limitRGB(double1.intValue()); 
			}
		}

		return matriz;
	}

	
	public int[][] getMatrizFiltroSobelHorizontal(int[][] origem) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);

		int[][] mascara = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};
		int largura = matriz.length;
		int altura = matriz[0].length;
		
		for (int linha = 1; linha < altura-1; linha++) {
			for (int coluna = 1; coluna < largura-1; coluna++) {
				int pixel = origem[coluna-1][linha-1]*mascara[0][0] + origem[coluna][linha-1]*mascara[0][1] + origem[coluna+1][linha-1]*mascara[0][2] + 
						origem[coluna-1][linha+0]*mascara[1][0] + origem[coluna][linha+0]*mascara[1][1] + origem[coluna+1][linha+0]*mascara[1][2] + 
						origem[coluna-1][linha+1]*mascara[2][0] + origem[coluna][linha+1]*mascara[2][1] + origem[coluna+1][linha+1]*mascara[2][2];
				pixel = Math.abs(pixel);
				matriz[coluna][linha] = VisaoUtils.limitRGB(pixel);
			}
		}

		return matriz;
	}

	public int[][] getMatrizFiltroSobelVertical(int[][] origem) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);

		int[][] mascara = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0,	1}};
		
		int largura = matriz.length;
		int altura = matriz[0].length;
		
		for (int linha = 1; linha < altura-1; linha++) {
			for (int coluna = 1; coluna < largura-1; coluna++) {
				int pixel = origem[coluna-1][linha-1]*mascara[0][0] + origem[coluna][linha-1]*mascara[0][1] + origem[coluna+1][linha-1]*mascara[0][2] + 
						origem[coluna-1][linha+0]*mascara[1][0] + origem[coluna][linha+0]*mascara[1][1] + origem[coluna+1][linha+0]*mascara[1][2] + 
						origem[coluna-1][linha+1]*mascara[2][0] + origem[coluna][linha+1]*mascara[2][1] + origem[coluna+1][linha+1]*mascara[2][2];
				pixel = Math.abs(pixel);
				matriz[coluna][linha] = VisaoUtils.limitRGB(pixel);
			}
		}

		return matriz;
	}

	public int[][] getMatrizFiltroSobel(int[][] origem) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);

		int[][] mascaraHorizontal = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};
		int[][] mascaraVertical = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0,	1}};
		
		int largura = matriz.length;
		int altura = matriz[0].length;
		
		for (int linha = 1; linha < altura-1; linha++) {
			for (int coluna = 1; coluna < largura-1; coluna++) {
				int pixelHorizontal = origem[coluna-1][linha-1]*mascaraHorizontal[0][0] + origem[coluna][linha-1]*mascaraHorizontal[0][1] + origem[coluna+1][linha-1]*mascaraHorizontal[0][2] + 
						origem[coluna-1][linha+0]*mascaraHorizontal[1][0] + origem[coluna][linha+0]*mascaraHorizontal[1][1] + origem[coluna+1][linha+0]*mascaraHorizontal[1][2] + 
						origem[coluna-1][linha+1]*mascaraHorizontal[2][0] + origem[coluna][linha+1]*mascaraHorizontal[2][1] + origem[coluna+1][linha+1]*mascaraHorizontal[2][2];
				pixelHorizontal = Math.abs(pixelHorizontal); 

				int pixelVertical = origem[coluna-1][linha-1]*mascaraVertical[0][0] + origem[coluna][linha-1]*mascaraVertical[0][1] + origem[coluna+1][linha-1]*mascaraVertical[0][2] + 
						origem[coluna-1][linha+0]*mascaraVertical[1][0] + origem[coluna][linha+0]*mascaraVertical[1][1] + origem[coluna+1][linha+0]*mascaraVertical[1][2] + 
						origem[coluna-1][linha+1]*mascaraVertical[2][0] + origem[coluna][linha+1]*mascaraVertical[2][1] + origem[coluna+1][linha+1]*mascaraVertical[2][2];
				pixelVertical = Math.abs(pixelVertical); 
				
				matriz[coluna][linha] = VisaoUtils.limitRGB((int) Math.sqrt(Math.pow(pixelHorizontal, 2)+Math.pow(pixelVertical, 2)));
			}
		}

		return matriz;
	}

	public int[][] getMatrizFiltroPrewitt(int[][] origem) {
		int[][] matriz = VisaoUtils.copyMatriz(origem);

		int[][] mascaraHorizontal = {{-1, -1, -1}, {0, 0, 0}, {1, 1, 1}};
		int[][] mascaraVertical = {{-1, 0, 1}, {-1, 0, 1}, {-1, 0,	1}};
		
		int largura = matriz.length;
		int altura = matriz[0].length;
		
		for (int linha = 1; linha < altura-1; linha++) {
			for (int coluna = 1; coluna < largura-1; coluna++) {
				int pixelHorizontal = origem[coluna-1][linha-1]*mascaraHorizontal[0][0] + origem[coluna][linha-1]*mascaraHorizontal[0][1] + origem[coluna+1][linha-1]*mascaraHorizontal[0][2] + 
						origem[coluna-1][linha+0]*mascaraHorizontal[1][0] + origem[coluna][linha+0]*mascaraHorizontal[1][1] + origem[coluna+1][linha+0]*mascaraHorizontal[1][2] + 
						origem[coluna-1][linha+1]*mascaraHorizontal[2][0] + origem[coluna][linha+1]*mascaraHorizontal[2][1] + origem[coluna+1][linha+1]*mascaraHorizontal[2][2];
				pixelHorizontal = Math.abs(pixelHorizontal); 

				int pixelVertical = origem[coluna-1][linha-1]*mascaraVertical[0][0] + origem[coluna][linha-1]*mascaraVertical[0][1] + origem[coluna+1][linha-1]*mascaraVertical[0][2] + 
						origem[coluna-1][linha+0]*mascaraVertical[1][0] + origem[coluna][linha+0]*mascaraVertical[1][1] + origem[coluna+1][linha+0]*mascaraVertical[1][2] + 
						origem[coluna-1][linha+1]*mascaraVertical[2][0] + origem[coluna][linha+1]*mascaraVertical[2][1] + origem[coluna+1][linha+1]*mascaraVertical[2][2];
				pixelVertical = Math.abs(pixelVertical); 
				
				matriz[coluna][linha] = VisaoUtils.limitRGB((int) Math.sqrt(Math.pow(pixelHorizontal, 2)+Math.pow(pixelVertical, 2)));
			}
		}

		return matriz;
	}

	
	// Exibe imagem apartir do path do arquivo
	public BufferedImage exibeImagem(String arquivo) {
		int[][][] arquivoMatrizRgb = this.getMatrizRgb(arquivo);
		return this.exibeImagem(arquivoMatrizRgb);
	}

	// Exibe imagem apartir de matriz RGB
	public BufferedImage exibeImagem(int[][][] origem) {
		int largura = origem.length;
		int altura = origem[0].length;
		BufferedImage imagem = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				int r = origem[coluna][linha][0];
				int g = origem[coluna][linha][1];
				int b = origem[coluna][linha][2];
				Color pixel = new Color(r, g, b);
				imagem.setRGB(coluna, linha, pixel.getRGB());
			}
		}
		return imagem;
	}

	// Exibe imagem apartir de matriz em tons de cinza ou preto e branco
	public BufferedImage exibeImagem(int[][] origem) {
		int largura = origem.length;
		int altura = origem[0].length;
		BufferedImage imagem = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
		for (int linha = 0; linha < altura; linha++) {
			for (int coluna = 0; coluna < largura; coluna++) {
				int cor = origem[coluna][linha];
				Color pixel = new Color(cor, cor, cor);
				imagem.setRGB(coluna, linha, pixel.getRGB());
			}
		}
		return imagem;
	}
}
