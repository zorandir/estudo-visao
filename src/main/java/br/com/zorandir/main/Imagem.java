package br.com.zorandir.main;

import br.com.zorandir.ui.Formulario;
import br.com.zorandir.visao.Visao;

public class Imagem {

	public static void main(String[] args) {
//		final String resourcesPath = "src/main/resources/imagem.png";
//		final String resourcesPath = "src/main/resources/imagem2.jpg";
//		final String resourcesPath = "src/main/resources/imagem3.jpg";
//		final String resourcesPath = "src/main/resources/imagem4.jpg";
		final String resourcesPath = "src/main/resources/imagem5.jpg";

		Visao visao = new Visao();
		Formulario f = new Formulario();

		int[][][] matrizOriginal = visao.getMatrizRgb(resourcesPath);
		int[][] matrizCinza = visao.getMatrizCinza(matrizOriginal);
		int[][] matrizCinzaEqualizada = visao.getMatrizFiltroEqualizador(matrizCinza);

		f.exibir(true, visao.exibeImagem(matrizCinza), visao.exibeImagem(matrizCinzaEqualizada));
	}
}
