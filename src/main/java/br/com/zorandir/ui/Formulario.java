package br.com.zorandir.ui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import br.com.zorandir.visao.Visao;

public class Formulario {

	public void exibir(BufferedImage... images) {
		this.exibir(false, images);
	}

	public void exibir(boolean histograma2, BufferedImage... images) {
		int hist = histograma2 ? 2 : 1;
		JFrame f = new JFrame("Fiap - Visão Computacional");
		GridLayout gridLayout = new GridLayout();
		if (images.length * hist == 1) {
			gridLayout.setRows(1);
			gridLayout.setColumns(1);
		} else if (images.length * hist == 2) {
			gridLayout.setRows(1);
			gridLayout.setColumns(2);
		} else {
			gridLayout.setRows(2);
			gridLayout.setColumns(3);
		}

		JPanel p = new JPanel(gridLayout);

		Visao visao = new Visao();

		for (BufferedImage image : images) {

			JLabel lblImagem1 = new JLabel();
			ImageIcon icon1 = new ImageIcon(image);
			lblImagem1.setIcon(icon1);

			p.add(lblImagem1);

			if (histograma2) {

				int[][][] matrizRgb = visao.getMatrizRgb(image);
				int[][] matrizCinza = visao.getMatrizCinza(matrizRgb);
				double[] histograma = visao.getHistograma(matrizCinza);

				JPanel chartPanel = criarChartPanel(histograma);
				Dimension maximumSize = new Dimension(image.getWidth(), image.getHeight());
				chartPanel.setMaximumSize(maximumSize);
				p.add(chartPanel);
			}
		}

		f.add(p);
		// f.add(chartPanel);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);

	}

	private static DefaultCategoryDataset crearDataset(double[] vector) {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (int i = 0; i < vector.length; i++) {
			dataset.setValue(vector[i], "Tons de Cinza", vector[i] + "");
		}
		return dataset;
	}

	private static JFreeChart criarChart(DefaultCategoryDataset dataset) {
		JFreeChart chart = ChartFactory.createBarChart("", null, null, dataset, PlotOrientation.VERTICAL, false, false, true);
		return chart;
	}

	private static JPanel criarChartPanel(double[] vector) {
		DefaultCategoryDataset crearDataset = crearDataset(vector);
		JFreeChart chart = criarChart(crearDataset);
		return new ChartPanel(chart);
	}

}
